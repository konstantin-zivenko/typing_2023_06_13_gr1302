"""PEP 544"""

from typing import Sized

def len(obj: Sized) -> int:
    return obj.__len__()
