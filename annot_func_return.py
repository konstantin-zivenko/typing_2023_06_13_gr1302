from typing import NoReturn

def play(player_name: str) -> None:
    print(f"{player_name} plays")


def black_hole() -> NoReturn:
    raise Exception("There is no going back ...")


ret_val = play("Petro")
